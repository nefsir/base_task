module NewLeadModal
  include PageObject

  text_field(:last_name_content, :id => "lead-last-name")
  button(:save_button, :css => ".save")
  def close_modal
    if cancel_task_element.visible?
      cancel_task
      task_modal_element.when_not_visible(3)
    end
  end
end
