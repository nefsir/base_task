require 'spec_helper'

shared_examples "Create lead and check it" do |content|
  it "redirect to new lead page" do
    on(LeadsPage).add_new_lead_element.when_visible(10).click
  end
  it "fill last name with provided content and add new lead" do
    on(NewLeadsPage).last_name_content_element.when_visible(10).value = content
    on(NewLeadsPage).save_button_element.when_visible(10).click
  end

  it "add note for lead and check it exists" do
    note_content = Faker::Lorem.sentence
    @current_page.div_element(:css => ".control-group").text_area_element.value = note_content
    @current_page.div_element(:css => ".control-group").button_element.click
    expect(@current_page.unordered_list_element(:css => ".feed-items").list_item_element(:css => ".note-activity").text == note_content)
  end

  it "perform cleanup" do
    @current_page.link_element(:css => ".detail-edit").click
    @current_page.link_element(:css => ".remove").click
  end

end

describe "Tasks" do
  before(:all) do
    login_to_autotest
    visit(LeadsPage)
  end

  describe "Add floating task" do
    let(:related_name) { nil }
    include_examples "Create lead and check it", Faker::Name.last_name
  end
end
